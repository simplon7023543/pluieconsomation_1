a) Trouver la rue qui a le plus de terrasses.
bdd(x) --> NICO
<- string requette, object cnx bdd
-> liste[{rue: , nbbar:}

f(x) -> ANTONY
<- listeliste[{rue: , nbbar:}]
-> [{rue: , nbbar:}] -> 1 element

b) Trouver les deux terrasses les plus proches (l’une de l’autre).
bdd(y) --> NICO
<- string requette (1 ou, on veux pour chaque coordonnees gps  count(*) > 1 la liste des bars), object cnx bdd
-> liste[?]

f(x) -> ANTONY
<- liste[?]
-> liste[{gps: , bars:[nombar]}]


c) Exporter un fichier Excel avec (nom de la rue, nombre de terrasses)

fxout(x) --> SEB
<- liste[dic], nom_fichier
-> fichier
